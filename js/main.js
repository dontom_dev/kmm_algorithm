/**
 * Algorithm:
 * Step 0:
 *      Display image and button.
 *      Create bitmap image variable.
 * 
 * Step 1:
 *	    Fill all matrix values with 0's.
 *      Mark image pixels as 1.
 *      Mark background pixels as 0.
 * 
 * Step 2:
 *      Skeletonization step: 
 *          Change one's sticking with background into two's and 
 *          in elbow corners into three's.
 * 
 * Step 3:
 *      Consider the points with 2, 3  
 * 		4 sticking neighbors and change them into 4’s. <- NOT DONE
 * 
 * Step 4: 
 *      Delete the 4’s. <- NOT DONE 
 * 
 * Step 5:
 *      Check for deleting the unnecessary 2’s and 3’s in the figure 
 *      above without interrupting the connectivity of the image.
 *      Repeat above is necessary.
 * 
 * Step 6: 
 *      Display final image with only 1's colored as black.
 */


// Static
var data;

// fingerprint
var canvasWidth = document.getElementById('canvasImage').getAttribute('width');
var canvasHeight = document.getElementById('canvasImage').getAttribute('height') + 1;
var imageSource = 'img/fingerprint.bmp';

// Two dimensional array for storing only 1's and 0's.
var bitArray;

// Create bitmap image variable.
var canvas = document.getElementById('canvasImage');
var context = canvas.getContext('2d');
var imageObj = loadImage(imageSource, context);

// Get KMM button object
var kmmButton = document.getElementById('kmmButton');




/************ FUNCTIONS ************/


/** 
 * Function creates image object and pixels array data.
 * @param {} imageSource
 * @param {} context
 * @returns {}
 */
function loadImage(imageSource, context) {
	var imageObj = new Image();

	imageObj.onload = function() {
		context.drawImage(imageObj, 0, 0)
	
		// Get all pixels from context
		var imageData = context.getImageData(0, 0, canvasWidth, canvasHeight);
		data = imageData.data;
	}
	imageObj.src = imageSource;

	return imageObj;
}


/**
* Draws final figured, consisted only with 1's and 0's
* Function changes all 1's into black color ad 0's into white.
*/
function drawFinalFigure() {
	for(var i = 0; i < canvasHeight; i++) {
		for(var j = 0; j < canvasWidth; j++) {
			if(bitArray[j][i] == 0) {
				context.fillStyle = "rgba(255,255,255,1)";
				context.fillRect(i, j, 1, 1);
			}
			else {
				context.fillStyle = "rgba(0,0,0,1)";
				context.fillRect(i, j, 1, 1);
			}
		}
	}
}


function TEST_CONSOLE(array, info) {
	var temp = "";

	array = fillZeros([canvasWidth, canvasHeight]);
	console.log(info);
	for(var i = 0; i < canvasHeight; i++) {
		temp = "";
		for(var j = 0; j < canvasWidth; j++){
			temp = temp + array[j][i] + " ";
		}
		console.log(temp);
	}
}


/**
* Function checks if there is any number inside given 2-dim matrix
* @param {Number} number is number which we look inside bitArray
* @param {Array} bitArray 2-dim array
* @param {Number} canvasWidth 
* @param {Number} canvasHeight 
* @returns {boolean} returns false if there is no occurance of number inside bitArray
*/
function hasNumber(number, bitArray, canvasWidth, canvasHeight) {
	for(var i = 0; i < canvasHeight; i++) {
		for(var j = 0; j < canvasWidth; j++) {
			if(bitArray[j][i] == number) {
				return true;
			}
		}
	}
	return false; // no 2's left
}


function thinnerOnes(bitArray, canvasWidth, canvasHeight) {
	for(var i = 0; i < canvasHeight; i++) {
		for(var j = 0; j < canvasWidth; j++) {
			if(bitArray[j][i] == 1) {
				var counterOfNeighborOnes = 0;
				// if 1 and more than 2 neighbors
				if(bitArray[j-1][i] == 1) {
					counterOfNeighborOnes++
				}
				if(bitArray[j+1][i] == 1) {
					counterOfNeighborOnes++
				}
				if(bitArray[j][i+1] == 1) {
					counterOfNeighborOnes++
				}
				if(bitArray[j][i-1] == 1) {
					counterOfNeighborOnes++
				}				
				if(bitArray[j-1][i-1] == 1) {
					counterOfNeighborOnes++
				}
				if(bitArray[j+1][i+1] == 1) {
					counterOfNeighborOnes++
				}
				if(bitArray[j-1][i+1] == 1) {
					counterOfNeighborOnes++
				}
				if(bitArray[j+1][i-1] == 1) {
					counterOfNeighborOnes++
				}
				if(counterOfNeighborOnes > 2) {
					return true;
				}
			}
		}
	}
	return false; // no more than 2's one's as neighbors
}

function countOfNeighborOnes(bitArray, i, j) {
	for(var y = -1; y <= 1; y++) {
		for(var x = -1; x <= 1; x++) {
			var counterOfNeighborOnes = 0;
			if(bitArray[j-1][i] == 1) {
				counterOfNeighborOnes++
			}
			if(bitArray[j+1][i] == 1) {
				counterOfNeighborOnes++
			}
			if(bitArray[j][i+1] == 1) {
				counterOfNeighborOnes++
			}
			if(bitArray[j][i-1] == 1) {
				counterOfNeighborOnes++
			}				
			if(bitArray[j-1][i-1] == 1) {
				counterOfNeighborOnes++
			}
			if(bitArray[j+1][i+1] == 1) {
				counterOfNeighborOnes++
			}
			if(bitArray[j-1][i+1] == 1) {
				counterOfNeighborOnes++
			}
			if(bitArray[j+1][i-1] == 1) {
				counterOfNeighborOnes++
			}
		}
	}
	if(counterOfNeighborOnes > 2) return true;
	return false;
}


/**
* Function is triggered when button is clicked. It runs all the action and 
* the most important is responsible for KMM algorithm implementation and logic.
*/
function runKmmAlgorithm() {
	// Create and fill array values with 0's.
	bitArray = fillZeros([canvasWidth, canvasHeight]);
	
	// Mark image black pixels as 1.
	bitArray = markOnes(data);

	// Change one's sticking with background into two's and in elbow corners into three's.
	bitArray = findTwosAndThrees();

	while(1) {
		// check if there are any 2's left
		// if not, then break
		if(!hasNumber(2, bitArray, canvasWidth, canvasHeight)) { // return false if there are no 2's left -> !false = true
			break;
		}
		
		for(var i = 1; i < canvasHeight; i++) {
			for(var j = 1; j < canvasWidth; j++) {
				if(bitArray[j][i] == 2) {

					for(var y = -1; y <= 1; y++) {
						for(var x = -1; x <= 1; x++) {

							// if 2 has 1 or 3 as a neighbor then delete this 2
							if(bitArray[j+x][i+y] == 1 || bitArray[j+x][i+y] == 3) {
								bitArray[j][i] = 0; // delete pixel
							}
							// if 2 has only 2 as a neighbor then change it into 1
							else if(bitArray[j+x][i+y] == 2) {
								bitArray[j][i] = 1;
							}

						}
					}

				}
			}
		}	
	}

	while(1) {
		// check if there are any 3's left
		// if no then break
		if(!hasNumber(3, bitArray, canvasWidth, canvasHeight)) { // return false if there are no 3's left -> !false = true
			break;
		}

		// if yes then repeat this =>

		for(var i = 1; i < canvasHeight; i++) {
			for(var j = 1; j < canvasWidth; j++) {
				if(bitArray[j][i] == 3) {

					for(var y = -1; y <= 1; y++) {
						for(var x = -1; x <= 1; x++) {

							// if 3 and neighbors are only 3 then delete
							if(bitArray[j+x][i+y] == 3) {
								bitArray[j][i] = 0;
							}
						}
					}

					for(var y = -1; y <= 1; y++) {
						for(var x = -1; x <= 1; x++) {

							// if 3 has neighbors only 3 then delete == no 1's
							if(bitArray[j+x][i+y] == 1) {
								bitArray[j][i] = 1; // delete pixel
							}

						}
					}

				}

			}
		}
	}

	while(1) {
		// check if there are any 1's that have more than 2 neighbors
		// if no then break
		if(!thinnerOnes(bitArray, canvasWidth, canvasHeight)) { // return false if there are not thin enough borders
			break;
		}

		// if yes then repeat this =>
		for(var i = 1; i < canvasHeight; i++) {
			for(var j = 1; j < canvasWidth; j++) {
				if(bitArray[j][i] == 1) {
					// if 1 and more than 2 neighbors on horizontal and vertical
					// change it to 0
					
					if(countOfNeighborOnes(bitArray, i, j)) {
						bitArray[j][i] = 0;
					}		

				}

			}
		}
	}
	drawFinalFigure();
}


/**
 * Function creates two dimensional array filled with one.
 * @param {Array} dimensions is array of width and height
 * @returns {Array}
 */
function fillZeros(dimensions) {
    var array = [];

    for (var i = 0; i < dimensions[0]; ++i) {
        array.push(dimensions.length == 1 ? 0 : fillZeros(dimensions.slice(1)));
    }
    return array;
}


/**
 * Function returns two dimensional array filled with 1's and 0's.
 * @param {Array} data is 1-dim array of original image pixels
 * @returns {Array} 
 */
function markOnes(data) {
	var bitArray = [], temp = []; 
	var red, green, blue;

	for(var i = 0, j = 0; i < data.length; i+=4, j++) {
		red = data[i];
		green = data[i + 1];
		blue = data[i + 2];
		
		if(red > 200 && green > 200 && blue > 200){
			temp[j] = 0; // if white
		}
		else { 
			temp[j] = 1; // almost black
		}
	}

	var z = 0
	for(var i = 0; i < canvasHeight; i++) {
		bitArray[i] = [];
		for(var j = 0; j < canvasWidth; j++,z++) {
			bitArray[i].push(temp[z]);
		}
	}
	// console.log("Marked ones: " + bitArray);

	return bitArray;
}


/**
* Function puts 2's to borders and 3's to corners.
* @param {Array} bitArray is 2-dim array filled with 0's and 1's
* @param {Number} j is width of final result array
* @param {Number} i is height of final result array
* @returns {} result
*/
function applyTwoOrThree(bitArray, j, i) {
	var result = 1;
	// if any neighbor is 0 on corner then return 3
	if(bitArray[j-1][i-1] == 0 || bitArray[j+1][i-1] == 0 || 
		bitArray[j-1][i+1] == 0 || bitArray[j+1][i+1] == 0) {
			result = 3;
	}

	// if any neighbor is 0 on horizontal or vertical then return 2
	if(bitArray[j-1][i] == 0 || bitArray[j+1][i] == 0 || 
		bitArray[j][i+1] == 0 || bitArray[j][i-1] == 0) {
			result = 2;
	}

	// if there is no neighbor of 0 then return 1
	return result;
}


/**
* Function finds all occurences of 1's as border
* @returns {Array} bitresult
*/
function findTwosAndThrees() {
	// console.log(bitArray);
	
	// skip border pixels
	for(var i = 1; i < canvasHeight; i++) {
		for(var j = 1; j < canvasWidth; j++) {
			if(bitArray[j][i] == 1) {
				bitArray[j][i] = applyTwoOrThree(bitArray, j, i);
			}
		}
	}
	// console.log(bitArray);
	return bitArray;
	// console.log(bitArray);
}

/************ END FUNCTIONS ************/