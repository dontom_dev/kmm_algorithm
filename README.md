#Title
KMM algorithm implementation

##Author
DonTom, Warsaw University of Technology Date: 09.01.2018

###Geting Started
Open up git shell and enter the following commands 
- npm install 
- bower install 
- grunt watch

And run the node server
- http-server

###Working program
http://dontom.pl/kmm_algorithm/

###Documentation
http://dontom.pl/kmm_algorithm/docs/Biometrics%20Report%203.pdf

###Repository
https://bitbucket.org/dontom_dev/kmm_algorithm